using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using AngleSharp.Common;
using Jackett.Common.Utils;

namespace Jackett.Common.Helpers
{
    public static class WebUtilityHelpers
    {
        public static string UrlEncode(string searchString, Encoding encoding)
        {
            if (string.IsNullOrEmpty(searchString))
            {
                return string.Empty;
            }

            var bytes = encoding.GetBytes(searchString);
            return encoding.GetString(WebUtility.UrlEncodeToBytes(bytes, 0, bytes.Length));
        }

        public static string UrlDecode(string searchString, Encoding encoding)
        {
            if (string.IsNullOrEmpty(searchString))
            {
                return string.Empty;
            }

            var inputBytes = encoding.GetBytes(searchString);
            return encoding.GetString(WebUtility.UrlDecodeToBytes(inputBytes, 0, inputBytes.Length));
        }

        public static string AddQueryString(string url, string key, string value)
        {
            
            return url + ((url.IndexOf('?') > 0) ? "&" : "?") + System.Web.HttpUtility.UrlEncode(key) + "=" + System.Web.HttpUtility.UrlEncode(value);

        }

        public static Dictionary<string, List<string>> ParseQueryString(string query)
        {
            var result = System.Web.HttpUtility.ParseQueryString(query);
            return result.ToEnumerable().ToDictionary(pair => pair.Key, pair => pair.Value.Split(',').ToList());
        }
    }
}
